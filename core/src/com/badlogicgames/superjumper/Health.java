package com.badlogicgames.superjumper;

/**
 * Created by Chbyto on 16/04/2017.
 */

public class Health extends GameObject {
    public static final float HEALTH_WIDTH = 0.5f;
    public static final float HEALTH_HEIGHT = 0.8f;
    public static final int HEALTH_SCORE = 1;

    float stateTime;

    public Health (float x, float y) {
        super(x, y, HEALTH_WIDTH, HEALTH_HEIGHT);
        stateTime = 0;
    }

    public void update (float deltaTime) {
        stateTime += deltaTime;
    }
}
