package com.badlogicgames.superjumper;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by xavier.belda on 22/03/2017.
 */
public class Particles extends ApplicationAdapter  {
    //////PARTICLE EFFECT
    //private ParticleEffect effect;
    SpriteBatch batch;
    public final Bob bob;

    public Particles(Bob bob) {
        this.bob = bob;
    }

    public void create()
    {
        //////////////
        batch = new SpriteBatch();
        //////////////
        Assets.effect = new ParticleEffect();
        Assets.effect.start();
    }
    public void update()
    {
        Assets.effect.setPosition(bob.position.x,bob.position.y);
    }
    public void render()
    {
        batch.begin();
        Assets.effect.draw(batch, Gdx.graphics.getDeltaTime());

        batch.end();
    }

}





